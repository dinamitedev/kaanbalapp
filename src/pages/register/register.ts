import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController  } from 'ionic-angular';
import {FormBuilder,Validators,FormGroup} from '@angular/forms';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import {LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  avatar=1;
  tipouser=1;
  puntos=10;
  RegistrarForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,private FormBuilder:FormBuilder,private api:UsuarioProvider,private alertCtrl: AlertController) {
  this.buildLoginForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  private buildLoginForm(){
    this.RegistrarForm= this.FormBuilder.group({
      nombre: ['', Validators.required],
      apellido:['', Validators.required],
      usuario:['', Validators.required],
      contrasena:['', Validators.required],
      correo:['', Validators.required],
      tipouser:['1', Validators.required],
      avatar:['1', Validators.required],
      puntos:['10', Validators.required],
    })
  }
  Registrar(){
    if(!this.RegistrarForm.valid){
      console.log('Campos vacios.'+this.RegistrarForm.value);
     }else{
     this.api.RegistrarUsuario(this.RegistrarForm.value).subscribe((data:any)=>{
      if(data.creado==='true'){
        this.buildLoginForm();
       
        let alert = this.alertCtrl.create({
          subTitle: data.mensaje,
          buttons: [
            {
              text: 'Ok',
              role: 'ok',
              handler: () => {
                console.log('atras');
                this.navCtrl.push(LoginPage);
              }
            }
          ]
        });
        alert.present();
        console.log(data.mensaje);
      }else{
        let alerterror = this.alertCtrl.create({
          subTitle: data.mensaje,
          buttons: ['Ok']
        });
        alerterror.present();
        console.log(data.mensaje);
      }

    },(error)=>{
      console.log(error);
    }
    );
     }
  }
  selectA1(){
  this.avatar=1;
  }
  selectA2(){
    this.avatar=2;
  }
  selectA3(){
    this.avatar=3;
  }
  selectA4(){
    this.avatar=4;
  }

}
