import { Component } from '@angular/core';
import {FormBuilder,Validators,FormGroup} from '@angular/forms';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import {MainPage} from "../main/main";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,private FormBuilder:FormBuilder,private api:UsuarioProvider,private loadingCtrl:LoadingController,private alertCtrl: AlertController) {
  this.buildLoginForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  private buildLoginForm(){
    this.loginForm= this.FormBuilder.group({
      usuario: ['', Validators.required],
      password:['', Validators.required]
    })
  }
  doLogin() {
   if(!this.loginForm.valid){
    console.log('Campos vacios.')
   }else{
    this.api.Login(this.loginForm.value).subscribe((data:any)=>{
      if(data.isLogin==='true'){
       
       
       //Guardar datos en storage
       window.localStorage.setItem('id',data.usuario['0']['id']);
       window.localStorage.setItem('nombre',data.usuario['0']['nombre']);
       window.localStorage.setItem('apellido',data.usuario['0']['apellido']);
       window.localStorage.setItem('usuario',data.usuario['0']['usuario']);
       window.localStorage.setItem('contrasena',data.usuario['0']['contrasena']);
       window.localStorage.setItem('correo',data.usuario['0']['correo']);
       window.localStorage.setItem('avatar',data.usuario['0']['avatar']);
       window.localStorage.setItem('puntos',data.usuario['0']['puntos']);

       let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: 'Cargando porfavor espere...'
      });
       loading.present();
       setTimeout(() => {
         this.navCtrl.setRoot('TabsPage');
       }, 1000);
       setTimeout(() => {
         loading.dismiss();
       }, 5000);
      }else{
        //Mostrar usuario o contrasena incorrectos
        let alerterror = this.alertCtrl.create({
          subTitle: data.mensaje,
          buttons: ['Ok']
        });
        alerterror.present();
      }

    },(error)=>{console.log(error)}
    
    );
   }
  }
  GoToRegistrar(){
    this.navCtrl.push(RegisterPage);
  }
  GoToMain(){
    this.navCtrl.push(MainPage);
  }

}
