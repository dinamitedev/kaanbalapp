import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  Nusuario='';
  Nombre='';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.Nusuario=window.localStorage.getItem('usuario');
    this.Nombre=window.localStorage.getItem('nombre');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
  }

}
