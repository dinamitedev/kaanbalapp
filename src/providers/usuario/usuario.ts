import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {
private API_URL='http://af1816c1.ngrok.io';
  constructor(public http: HttpClient) {
    console.log('Conectando con el servidor de kaanbal');
  }
  Login(dato:any){
    let datos={usuario:dato.usuario,password:dato.password}
    let options={
      headers:{
        'Access-Control-Allow-Origin':'*',
        'Access-Control-Allow-Methoods':'POST,GET,OPTIONS,PUT',
        'Accept':'application/json',
        'Content-Type':'application/json'
      }
    };
    var url =this.API_URL+'/api/user/login';
    return this.http.post(url,JSON.stringify(datos),options);
  }

  RegistrarUsuario(dato:any){
    let datos={nombre:dato.nombre,apellido:dato.apellido,usuario:dato.usuario,password:dato.contrasena,correo:dato.correo,avatar:dato.avatar,tipouser:dato.tipouser,puntos:dato.puntos}
    let options={
      headers:{
        'Access-Control-Allow-Origin':'*',
        'Access-Control-Allow-Methoods':'POST,GET,OPTIONS,PUT',
        'Accept':'application/json',
        'Content-Type':'application/json'

      }
    };
    var url=this.API_URL+'/user/registrar';
    console.log(datos);

    return  this.http.post(url,JSON.stringify(datos),options);
  }


}
